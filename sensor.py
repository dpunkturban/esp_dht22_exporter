import socket
import time
from prometheus_client import start_http_server, Summary, Histogram, Gauge

UDP_IP_ADDR = "192.168.0.12"
UDP_PORT_NO = 6789
UDP_IP_ADDR_CLIENT = "192.168.0.64"
UDP_PORT_NO_CLIENT = 4210

# declare our serverSocket upon which
# we will be listening for UDP messages
serverSock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
# One difference is that we will have to bind our declared IP address
# and port number to our newly declared serverSock
serverSock.bind((UDP_IP_ADDR, UDP_PORT_NO))

# Create a metric to track time spent and requests made.
REQUEST_TIME = Summary('request_processing_seconds', 'Time spent processing request')
h = Gauge('humidity', 'Luftfeuchtigkeit')
t = Gauge('temperature', 'Temperatur')


if __name__ == '__main__':
    # Start up the server to expose the metrics.
    start_http_server(8123)
    # Generate some requests.

    humidity = 0
    temp = 0

    while True:
        time.sleep(60)
        

        clientSock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        clientSock.sendto(b'Hello World', (b'192.168.0.64', 4210))

        data, addr = serverSock.recvfrom(1024)
        
        if "Humidity" in str(data):
            tmp_data = str(data)
            humidity = tmp_data[11:16]
            if "nan" not in str(data):
                h.set(float(humidity))

        elif "Temperature" in str(data):
            tmp_data = str(data)
            temperature = tmp_data[15:20]
            if "nan" not in str(data):
                t.set(float(temperature))

